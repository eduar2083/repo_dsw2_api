package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "common.Pais")
public class PaisBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PaisId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int paisId;
	
	@Column(name = "CodigoISO")
	private String codigoISO;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@OneToMany(mappedBy = "nacionalidad")
	@JsonIgnore
	private List<CiudadanoBean> ciudadanos;

	public PaisBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaisBean(String codigoISO, String nombre, boolean eliminado) {
		super();
		this.codigoISO = codigoISO;
		this.nombre = nombre;
		this.eliminado = eliminado;
	}

	public int getPaisId() {
		return paisId;
	}

	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}

	public String getCodigoISO() {
		return codigoISO;
	}

	public void setCodigoISO(String codigoISO) {
		this.codigoISO = codigoISO;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public List<CiudadanoBean> getCiudadanos() {
		return ciudadanos;
	}

	public void setCiudadanos(List<CiudadanoBean> ciudadanos) {
		this.ciudadanos = ciudadanos;
	}
}

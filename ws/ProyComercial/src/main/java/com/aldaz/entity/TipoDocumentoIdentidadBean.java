package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "common.TipoDocumento")
public class TipoDocumentoIdentidadBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TipoDocumentoIdentidadId")
	private int tipoDocumentoIdentidadId;
	
	@Column(name = "DescripcionLarga")
	private String descripcionLarga;
	
	@Column(name = "DescripcionCorta")
	private String descripcionCorta;
	
	@OneToMany(mappedBy = "tipoDocumento")
	@JsonIgnore
	private List<CiudadanoBean> ciudadanos;
	

	public TipoDocumentoIdentidadBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipoDocumentoIdentidadBean(String descripcionLarga, String descripcionCorta) {
		super();
		this.descripcionLarga = descripcionLarga;
		this.descripcionCorta = descripcionCorta;
	}

	public int getTipoDocumentoIdentidadId() {
		return tipoDocumentoIdentidadId;
	}

	public void setTipoDocumentoIdentidadId(int tipoDocumentoIdentidadId) {
		this.tipoDocumentoIdentidadId = tipoDocumentoIdentidadId;
	}

	public String getDescripcionLarga() {
		return descripcionLarga;
	}

	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}

	public String getDescripcionCorta() {
		return descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public List<CiudadanoBean> getCiudadanos() {
		return ciudadanos;
	}

	public void setCiudadanos(List<CiudadanoBean> ciudadanos) {
		this.ciudadanos = ciudadanos;
	}
}

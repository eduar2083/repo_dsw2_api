package com.aldaz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "covid.Contacto")
public class ContactoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ContactoId")
	private int contactoId;
	
	@Column(name = "nombreContacto")
	private String nombreCompleto;
	
	@Column(name = "Parentesco")
	private String parentesco;
	
	@Column(name = "Telefono")
	private String telefono;
	
	@Column(name = "Correo")
	private String correo;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "CiudadanoId")
	@ManyToOne
	private CiudadanoBean ciudadano;

	public ContactoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContactoBean(String nombreCompleto, String parentesco, String telefono, String correo, boolean eliminado,
			CiudadanoBean ciudadano) {
		super();
		this.nombreCompleto = nombreCompleto;
		this.parentesco = parentesco;
		this.telefono = telefono;
		this.correo = correo;
		this.eliminado = eliminado;
		this.ciudadano = ciudadano;
	}

	public int getContactoId() {
		return contactoId;
	}

	public void setContactoId(int contactoId) {
		this.contactoId = contactoId;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public CiudadanoBean getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(CiudadanoBean ciudadano) {
		this.ciudadano = ciudadano;
	}
}

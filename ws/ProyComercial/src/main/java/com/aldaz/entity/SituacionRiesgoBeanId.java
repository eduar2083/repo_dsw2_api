package com.aldaz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SituacionRiesgoBeanId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "SituacionRiesgoId")
	private int situacionRiesgoId;
	
	@Column(name = "CiudadanoId")
	private int ciudadanoId;

	public SituacionRiesgoBeanId(int situacionRiesgoId, int ciudadanoId) {
		super();
		this.situacionRiesgoId = situacionRiesgoId;
		this.ciudadanoId = ciudadanoId;
	}
}

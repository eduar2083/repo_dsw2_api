package com.aldaz.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "covid.Sintoma")
public class SintomaBean {
	
	@Column(name = "SintomaId", insertable = false, updatable = false)
	private int sintomaId;
	
	@Column(name = "Eliminado")
	private int eliminado;
	
	@JoinColumn(name = "CiudadanoId", insertable = false, updatable = false)
	@ManyToOne
	private CiudadanoBean ciudadano;

	@EmbeddedId
	private SintomaBeanId id;	// PK

	public SintomaBeanId getId() {
		return id;
	}

	public void setId(SintomaBeanId id) {
		this.id = id;
	}

	public int getSintomaId() {
		return sintomaId;
	}

	public void setSintomaId(int sintomaId) {
		this.sintomaId = sintomaId;
	}

	public int getEliminado() {
		return eliminado;
	}

	public void setEliminado(int eliminado) {
		this.eliminado = eliminado;
	}

	public CiudadanoBean getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(CiudadanoBean ciudadano) {
		this.ciudadano = ciudadano;
	}
}

package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "covid.Estado")
public class EstadoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EstadoId")
	private int estadoId;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Descripcion")
	private String descripcion;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@OneToMany(mappedBy = "estado")
	@JsonIgnore
	private List<CiudadanoBean> ciudadanos;

	public EstadoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EstadoBean(String nombre, String descripcion, boolean eliminado) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.eliminado = eliminado;
	}

	public int getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(int estadoId) {
		this.estadoId = estadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public List<CiudadanoBean> getCiudadanos() {
		return ciudadanos;
	}

	public void setCiudadanos(List<CiudadanoBean> ciudadanos) {
		this.ciudadanos = ciudadanos;
	}
}

package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "common.Provincia")
public class ProvinciaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ProvinciaId")
	private int provinciaId;
	
	@Column(name = "Ubigeo")
	private String ubigeo;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "DepartamentoId")
	@ManyToOne
	private DepartamentoBean departamento;
	
	@OneToMany(mappedBy = "provincia")
	@JsonIgnore
	private List<DistritoBean> distritos;

	public ProvinciaBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProvinciaBean(String ubigeo, String nombre, DepartamentoBean departamento) {
		super();
		this.ubigeo = ubigeo;
		this.nombre = nombre;
		this.departamento = departamento;
	}

	public int getProvinciaId() {
		return provinciaId;
	}

	public void setProvinciaId(int provinciaId) {
		this.provinciaId = provinciaId;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public DepartamentoBean getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoBean departamento) {
		this.departamento = departamento;
	}

	public List<DistritoBean> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<DistritoBean> distritos) {
		this.distritos = distritos;
	}
}

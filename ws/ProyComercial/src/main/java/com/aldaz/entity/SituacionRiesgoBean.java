package com.aldaz.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "covid.SituacionRiesgo")
public class SituacionRiesgoBean {
	
	@Column(name = "SituacionRiesgoId", insertable = false, updatable = false)
	private int situacionRiesgoId;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "CiudadanoId", insertable = false, updatable = false)
	@ManyToOne
	private CiudadanoBean ciudadano;

	@EmbeddedId
	private SituacionRiesgoBeanId id;	// PK

	public SituacionRiesgoBeanId getId() {
		return id;
	}

	public void setId(SituacionRiesgoBeanId id) {
		this.id = id;
	}

	public int getSituacionRiesgoId() {
		return situacionRiesgoId;
	}

	public void setSituacionRiesgoId(int situacionRiesgoId) {
		this.situacionRiesgoId = situacionRiesgoId;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public CiudadanoBean getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(CiudadanoBean ciudadano) {
		this.ciudadano = ciudadano;
	}
}

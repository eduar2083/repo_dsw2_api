package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "common.Departamento")
public class DepartamentoBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DepartamentoId")
	private int departamentoId;
	
	@Column(name = "Ubigeo")
	private String ubigeo;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Eliminado")
	private boolean Eliminado;
	
	@OneToMany(mappedBy = "departamento")
	@JsonIgnore
	private List<ProvinciaBean> provincias;

	public DepartamentoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DepartamentoBean(String ubigeo, String nombre, boolean eliminado) {
		super();
		this.ubigeo = ubigeo;
		this.nombre = nombre;
		Eliminado = eliminado;
	}

	public int getDepartamentoId() {
		return departamentoId;
	}

	public void setDepartamentoId(int departamentoId) {
		this.departamentoId = departamentoId;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEliminado() {
		return Eliminado;
	}

	public void setEliminado(boolean eliminado) {
		Eliminado = eliminado;
	}

	public List<ProvinciaBean> getProvincias() {
		return provincias;
	}

	public void setProvincias(List<ProvinciaBean> provincias) {
		this.provincias = provincias;
	}
}

package com.aldaz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Point;

@Entity
@Table(name = "covid.Direccion")
public class DireccionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DireccionId")
	private int direccionId;
	
	@Column(name = "Descripcion")
	private String descripcion;
	
	@Column(name = "Ubicacion")
	private Point ubicacion;

	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "DistritoId")
	@ManyToOne
	private DistritoBean distrito;
	
	@JoinColumn(name = "CiudadanoId")
	@ManyToOne
	private CiudadanoBean ciudadano;

	public DireccionBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DireccionBean(String descripcion, Point ubicacion, boolean eliminado, DistritoBean distrito,
			CiudadanoBean ciudadano) {
		super();
		this.descripcion = descripcion;
		this.ubicacion = ubicacion;
		this.eliminado = eliminado;
		this.distrito = distrito;
		this.ciudadano = ciudadano;
	}



	public int getDireccionId() {
		return direccionId;
	}

	public void setDireccionId(int direccionId) {
		this.direccionId = direccionId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Point getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Point ubicacion) {
		this.ubicacion = ubicacion;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public DistritoBean getDistrito() {
		return distrito;
	}

	public void setDistrito(DistritoBean distrito) {
		this.distrito = distrito;
	}

	public CiudadanoBean getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(CiudadanoBean ciudadano) {
		this.ciudadano = ciudadano;
	}	
}

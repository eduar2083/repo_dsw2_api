package com.aldaz.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "covid.Ciudadano")
public class CiudadanoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "CiudadanoId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int ciudadanoId;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "ApellidoPaterno")
	private String apellidoPaterno;
	
	@Column(name = "ApellidoMaterno")
	private String apellidoMaterno;
	
	@Column(name = "Sexo")
	private boolean sexo;
	
	@Column(name = "Celular")
	private String celular;
	
	@Column(name = "Correo")
	private String correo;
	
	@Column(name = "FechaNacimiento")
	private Date fechaNacimiento;
	
	@Column(name = "NroDocumentoIdentidad")
	private String nroDocumentoIdentidad;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "NacionalidadId")
	@ManyToOne
	private PaisBean nacionalidad;
	
	@JoinColumn(name = "TipoDocumentoIdentidadId")
	@ManyToOne
	private TipoDocumentoIdentidadBean tipoDocumento;
	
	@JoinColumn(name = "EstadoId")
	@ManyToOne
	private EstadoBean estado;
	
	@OneToMany(mappedBy = "ciudadano")
	@JsonIgnore
	private List<DireccionBean> direcciones;
	
	@OneToMany(mappedBy = "ciudadano")
	@JsonIgnore
	private List<ContactoBean> contactos;
	
	@OneToMany(mappedBy = "ciudadano")
	@JsonIgnore
	private List<SintomaBean> sintomas;
	
	@OneToMany(mappedBy = "ciudadano")
	@JsonIgnore
	private List<SituacionRiesgoBean> situacionesRiesgo;
	
	public CiudadanoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CiudadanoBean(String nombre, String apellidoPaterno, String apellidoMaterno, boolean sexo, String celular,
			String correo, Date fechaNacimiento, PaisBean nacionalidad, TipoDocumentoIdentidadBean tipoDocumento,
			EstadoBean estado) {
		super();
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.sexo = sexo;
		this.celular = celular;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento;
		this.nacionalidad = nacionalidad;
		this.tipoDocumento = tipoDocumento;
		this.estado = estado;
	}
	
	public int getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public boolean isSexo() {
		return sexo;
	}

	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNroDocumentoIdentidad() {
		return nroDocumentoIdentidad;
	}

	public void setNroDocumentoIdentidad(String nroDocumentoIdentidad) {
		this.nroDocumentoIdentidad = nroDocumentoIdentidad;
	}

	public PaisBean getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(PaisBean nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public TipoDocumentoIdentidadBean getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoIdentidadBean tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public EstadoBean getEstado() {
		return estado;
	}

	public void setEstado(EstadoBean estado) {
		this.estado = estado;
	}

	public List<DireccionBean> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<DireccionBean> direcciones) {
		this.direcciones = direcciones;
	}

	public List<ContactoBean> getContactos() {
		return contactos;
	}

	public void setContactos(List<ContactoBean> contactos) {
		this.contactos = contactos;
	}

	public List<SintomaBean> getSintomas() {
		return sintomas;
	}

	public void setSintomas(List<SintomaBean> sintomas) {
		this.sintomas = sintomas;
	}

	public List<SituacionRiesgoBean> getSituacionesRiesgo() {
		return situacionesRiesgo;
	}

	public void setSituacionesRiesgo(List<SituacionRiesgoBean> situacionesRiesgo) {
		this.situacionesRiesgo = situacionesRiesgo;
	}
}

package com.aldaz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "common.Distrito")
public class DistritoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DistritoId")
	private int distritoId;
	
	@Column(name = "Ubigeo")
	private String ubigeo;
	
	@Column(name = "Nombre")
	private String nombre;
	
	@Column(name = "Eliminado")
	private boolean eliminado;
	
	@JoinColumn(name = "ProvinciaId")
	@ManyToOne
	private ProvinciaBean provincia;
	
	@OneToMany(mappedBy = "distrito")
	@JsonIgnore
	private List<DireccionBean> direcciones;

	public DistritoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DistritoBean(String ubigeo, String nombre, boolean eliminado, ProvinciaBean provincia) {
		super();
		this.ubigeo = ubigeo;
		this.nombre = nombre;
		this.eliminado = eliminado;
		this.provincia = provincia;
	}

	public int getDistritoId() {
		return distritoId;
	}

	public void setDistritoId(int distritoId) {
		this.distritoId = distritoId;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public ProvinciaBean getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaBean provincia) {
		this.provincia = provincia;
	}

	public List<DireccionBean> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<DireccionBean> direcciones) {
		this.direcciones = direcciones;
	}
}

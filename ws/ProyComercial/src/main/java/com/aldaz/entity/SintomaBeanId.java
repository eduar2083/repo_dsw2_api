package com.aldaz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SintomaBeanId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "SintomaId")
	private int sintomaId;
	
	@Column(name = "CiudadanoId")
	private int ciudadanoId;

	public SintomaBeanId(int sintomaId, int ciudadanoId) {
		super();
		this.sintomaId = sintomaId;
		this.ciudadanoId = ciudadanoId;
	}

	public int getSintomaId() {
		return sintomaId;
	}

	public void setSintomaId(int sintomaId) {
		this.sintomaId = sintomaId;
	}

	public int getCiudadanoId() {
		return ciudadanoId;
	}

	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}
}

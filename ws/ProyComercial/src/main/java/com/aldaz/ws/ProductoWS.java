package com.aldaz.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.entity.ProductoBean;
import com.aldaz.service.ProductoService;

@Path("producto")
public class ProductoWS {
	
	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	
	private ProductoService productoService = ctx.getBean("productoServiceImpl", ProductoService.class);
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertar(ProductoBean bean) {
		try {
			return Response.ok(productoService.insertar(bean)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizar(ProductoBean bean) {
		try {
			productoService.actualizar(bean);
			
			return Response.noContent().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Path("/{id}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response eliminar(@PathParam("id") int idProducto) {
		try {
			productoService.eliminar(idProducto);
			
			return Response.noContent().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerPorId(@PathParam("id") int idProducto) {
		try {
			return Response.ok(productoService.obtenerPorId(idProducto)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar() {
		try {
			return Response.ok(productoService.listar()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Path("/filtrar/{min}/{max}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response filtrarXPrecio(@PathParam("min") double minimo, @PathParam("max") double maximo) {
		try {
			return Response.ok(productoService.filtrarXPrecio(minimo, maximo)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

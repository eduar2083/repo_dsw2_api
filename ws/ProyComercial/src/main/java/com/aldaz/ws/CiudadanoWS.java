package com.aldaz.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.dto.CiudadanoTranDto;
import com.aldaz.entity.CiudadanoBean;
import com.aldaz.service.CiudadanoService;

@Path("ciudadanos")
public class CiudadanoWS {

	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	private CiudadanoService service = ctx.getBean("ciudadanoServiceImpl", CiudadanoService.class);
	
	@Path("/filtrar/{nombre:.*}/{nroDocumentoIdentidad:.*}/{estadoId:.*}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar(@PathParam("nombre") String nombre, @PathParam("nroDocumentoIdentidad") String nroDocumentoIdentidad, @PathParam("estadoId") int estadoId) {
		try {
			List<CiudadanoBean> lista = service.filtrar(nombre, nroDocumentoIdentidad, estadoId);
			return Response.ok(lista).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Path("/{ciudadanoId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtener(@PathParam("ciudadanoId") int ciudadanoId) {
		try {
			CiudadanoBean entidad = service.obtener(ciudadanoId);
			return Response.ok(entidad).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertar(CiudadanoTranDto dto) {
		try {
			return Response.ok(service.insertar(dto)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizar(CiudadanoTranDto dto) {
		try {
			service.actualizar(dto);
			
			return Response.noContent().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

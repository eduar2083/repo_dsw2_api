package com.aldaz.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.entity.DistritoBean;
import com.aldaz.service.DistritoService;

@Path("distritos")
public class DistritoWS {

	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	private DistritoService service = ctx.getBean("distritoServiceImpl", DistritoService.class);
	
	@Path("/{provinciaId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar(@PathParam("provinciaId") int provinciaId) {
		try {
			List<DistritoBean> lista = service.listarXProvincia(provinciaId);
			return Response.ok(lista).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

package com.aldaz.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.aldaz.entity.UsuarioBean;

@Path("usuarios")
public class UsuarioWS {

	@Path("/validar")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response validar(UsuarioBean p) {
		try {
			if (p.getUsername().equals("ealdaz") && p.getPassword().equals("123456")) {
				p.setValido(true);
			}
			else {
				p.setValido(false);
			}
			
			return Response.ok(p).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

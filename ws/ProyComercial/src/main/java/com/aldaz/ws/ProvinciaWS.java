package com.aldaz.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.entity.ProvinciaBean;
import com.aldaz.service.ProvinciaService;

@Path("provincias")
public class ProvinciaWS {

	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	private ProvinciaService service = ctx.getBean("provinciaServiceImpl", ProvinciaService.class);
	
	@Path("/{departamentoId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar(@PathParam("departamentoId") int departamentoId) {
		try {
			List<ProvinciaBean> lista = service.listarXDepartamento(departamentoId);
			return Response.ok(lista).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

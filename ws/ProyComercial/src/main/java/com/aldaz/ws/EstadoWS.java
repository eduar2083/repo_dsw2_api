package com.aldaz.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.entity.EstadoBean;
import com.aldaz.service.EstadoService;

@Path("estados")
public class EstadoWS {

	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	private EstadoService service = ctx.getBean("estadoServiceImpl", EstadoService.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar() {
		try {
			List<EstadoBean> lista = service.listar();
			return Response.ok(lista).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

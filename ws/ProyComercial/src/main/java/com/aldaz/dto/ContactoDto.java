package com.aldaz.dto;

public class ContactoDto {
	private int contactoId;
	private int ciudadanoId;
	private String nombreCompleto;
	private String parentesco;
	private String telefono;
	private String correo;
	
	public int getContactoId() {
		return contactoId;
	}
	public void setContactoId(int contactoId) {
		this.contactoId = contactoId;
	}
	public int getCiudadanoId() {
		return ciudadanoId;
	}
	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getParentesco() {
		return parentesco;
	}
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
}

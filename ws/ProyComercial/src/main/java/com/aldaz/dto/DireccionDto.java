package com.aldaz.dto;

public class DireccionDto {
	private int direccionId;
	private int distritoId;
	private String descripcion;
	private double latitud;
	private double longitud;
	private int ciudadanoId;
	
	public int getDireccionId() {
		return direccionId;
	}
	public void setDireccionId(int direccionId) {
		this.direccionId = direccionId;
	}
	public int getDistritoId() {
		return distritoId;
	}
	public void setDistritoId(int distritoId) {
		this.distritoId = distritoId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public int getCiudadanoId() {
		return ciudadanoId;
	}
	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}
}

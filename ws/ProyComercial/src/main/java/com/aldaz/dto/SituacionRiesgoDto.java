package com.aldaz.dto;

public class SituacionRiesgoDto {
	private int situacionRiesgoId;
	private int ciudadanoId;
	
	public int getSituacionRiesgoId() {
		return situacionRiesgoId;
	}
	public void setSituacionRiesgoId(int situacionRiesgoId) {
		this.situacionRiesgoId = situacionRiesgoId;
	}
	public int getCiudadanoId() {
		return ciudadanoId;
	}
	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}
}

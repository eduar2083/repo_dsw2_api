package com.aldaz.dto;

public class SintomaDto {
	private int sintomaId;
	private int ciudadanoId;
	
	public int getSintomaId() {
		return sintomaId;
	}
	public void setSintomaId(int sintomaId) {
		this.sintomaId = sintomaId;
	}
	public int getCiudadanoId() {
		return ciudadanoId;
	}
	public void setCiudadanoId(int ciudadanoId) {
		this.ciudadanoId = ciudadanoId;
	}
}

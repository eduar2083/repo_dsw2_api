package com.aldaz.dto;

import java.util.List;

public class CiudadanoTranDto {
	
	public CiudadanoDto ciudadano;
	
	public List<ContactoDto> contactos;
	
	public List<DireccionDto> direcciones;
	
	public List<SintomaDto> sintomas;
	
	public List<SituacionRiesgoDto> situacionesRiesgo;

	public CiudadanoTranDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CiudadanoTranDto(CiudadanoDto ciudadano, List<ContactoDto> contactos, List<DireccionDto> direcciones,
			List<SintomaDto> sintomas, List<SituacionRiesgoDto> situacionesRiesgo) {
		super();
		this.ciudadano = ciudadano;
		this.contactos = contactos;
		this.direcciones = direcciones;
		this.sintomas = sintomas;
		this.situacionesRiesgo = situacionesRiesgo;
	}

	public CiudadanoDto getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(CiudadanoDto ciudadano) {
		this.ciudadano = ciudadano;
	}

	public List<ContactoDto> getContactos() {
		return contactos;
	}

	public void setContactos(List<ContactoDto> contactos) {
		this.contactos = contactos;
	}

	public List<DireccionDto> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<DireccionDto> direcciones) {
		this.direcciones = direcciones;
	}

	public List<SintomaDto> getSintomas() {
		return sintomas;
	}

	public void setSintomas(List<SintomaDto> sintomas) {
		this.sintomas = sintomas;
	}

	public List<SituacionRiesgoDto> getSituacionesRiesgo() {
		return situacionesRiesgo;
	}

	public void setSituacionesRiesgo(List<SituacionRiesgoDto> situacionesRiesgo) {
		this.situacionesRiesgo = situacionesRiesgo;
	}
}

package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.EstadoBean;

public interface EstadoService {

	public List<EstadoBean> listar();
}

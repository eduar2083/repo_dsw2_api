package com.aldaz.service;

import java.util.List;

import com.aldaz.dto.CiudadanoTranDto;
import com.aldaz.entity.CiudadanoBean;

public interface CiudadanoService {

	public List<CiudadanoBean> filtrar(String nombre, String nroDocumentoIdentidad, int estadoId);
	
	public CiudadanoBean obtener(int ciudadanoId);
	
	public CiudadanoBean insertar(CiudadanoTranDto dto);
	
	public CiudadanoBean actualizar(CiudadanoTranDto dto);
}

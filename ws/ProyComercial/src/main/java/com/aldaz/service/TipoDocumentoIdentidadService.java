package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.TipoDocumentoIdentidadBean;

public interface TipoDocumentoIdentidadService {

	public List<TipoDocumentoIdentidadBean> listar();
}

package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.PaisBean;

public interface PaisService {
	public List<PaisBean> listar();
}

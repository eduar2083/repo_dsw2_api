package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.ProductoDAO;
import com.aldaz.entity.ProductoBean;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	@Autowired
	private ProductoDAO dao;
	
	public ProductoBean insertar(ProductoBean bean) {
		return dao.insertar(bean);
	}
	
	public void actualizar(ProductoBean bean) {
		dao.actualizar(bean);
	}

	public ProductoBean obtenerPorId(int idProducto) {
		return dao.obtenerPorId(idProducto);
	}

	public void eliminar(int idProducto) {
		dao.eliminar(idProducto);
	}

	public List<ProductoBean> listar() {
		return dao.listar();
	}
	
	public List<ProductoBean> filtrarXPrecio(double minimo, double maximo) {
		return dao.filtrarXPrecio(minimo, maximo);
	}
}

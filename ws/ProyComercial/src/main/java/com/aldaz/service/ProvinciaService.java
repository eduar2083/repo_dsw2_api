package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.ProvinciaBean;

public interface ProvinciaService {

	public List<ProvinciaBean> listarXDepartamento(int departamentoId);
}

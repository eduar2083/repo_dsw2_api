package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.EstadoDAO;
import com.aldaz.entity.EstadoBean;

@Service
public class EstadoServiceImpl implements EstadoService {

	private @Autowired EstadoDAO dao;
	
	public List<EstadoBean> listar() {
		return dao.listar();
	}

}

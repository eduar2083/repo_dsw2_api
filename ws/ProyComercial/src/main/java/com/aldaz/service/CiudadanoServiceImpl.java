package com.aldaz.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.CiudadanoDAO;
import com.aldaz.dto.CiudadanoTranDto;
import com.aldaz.dto.ContactoDto;
import com.aldaz.dto.DireccionDto;
import com.aldaz.dto.SintomaDto;
import com.aldaz.dto.SituacionRiesgoDto;
import com.aldaz.entity.CiudadanoBean;
import com.aldaz.entity.ContactoBean;
import com.aldaz.entity.DireccionBean;
import com.aldaz.entity.EstadoBean;
import com.aldaz.entity.PaisBean;
import com.aldaz.entity.SintomaBean;
import com.aldaz.entity.SintomaBeanId;
import com.aldaz.entity.SituacionRiesgoBean;
import com.aldaz.entity.SituacionRiesgoBeanId;
import com.aldaz.entity.TipoDocumentoIdentidadBean;
import com.aldaz.util.GeometriesFactory;

@Service
public class CiudadanoServiceImpl implements CiudadanoService {
	
	private @Autowired CiudadanoDAO dao;

	public List<CiudadanoBean> filtrar(String nombre, String nroDocumentoIdentidad, int estadoId) {
		return dao.filtrar(nombre, nroDocumentoIdentidad, estadoId);
	}

	public CiudadanoBean obtener(int ciudadanoId) {
		return dao.obtener(ciudadanoId);
	}

	public CiudadanoBean insertar(CiudadanoTranDto dto) {
		CiudadanoBean ciudadano = new CiudadanoBean();
		ciudadano.setApellidoPaterno(dto.getCiudadano().getApellidoPaterno());
		ciudadano.setApellidoMaterno(dto.getCiudadano().getApellidoMaterno());
		ciudadano.setNombre(dto.getCiudadano().getNombre());
		ciudadano.setSexo(dto.getCiudadano().isSexo());
		
		PaisBean nacionalidad = new PaisBean();
		nacionalidad.setPaisId(dto.getCiudadano().getNacionalidadId());
		ciudadano.setNacionalidad(nacionalidad);
		
		TipoDocumentoIdentidadBean tipoDocumento = new TipoDocumentoIdentidadBean();
		tipoDocumento.setTipoDocumentoIdentidadId(dto.getCiudadano().getTipoDocumentoIdentidadId());
		ciudadano.setTipoDocumento(tipoDocumento);
		
		ciudadano.setNroDocumentoIdentidad(dto.getCiudadano().getNroDocumentoIdentidadId());
		ciudadano.setCelular(dto.getCiudadano().getCelular());
		ciudadano.setCorreo(dto.getCiudadano().getCorreo());
		ciudadano.setFechaNacimiento(dto.getCiudadano().getFechaNacimiento());
		
		EstadoBean estado = new EstadoBean();
		estado.setEstadoId(dto.getCiudadano().getEstadoId());
		ciudadano.setEstado(estado);
		
		List<ContactoBean> contactos = new ArrayList<ContactoBean>();
		for (ContactoDto c : dto.getContactos()) {
			ContactoBean bean = new ContactoBean();
			bean.setContactoId(c.getContactoId());
			bean.setNombreCompleto(c.getNombreCompleto());
			bean.setParentesco(c.getParentesco());
			bean.setTelefono(c.getTelefono());
			bean.setCorreo(c.getCorreo());
			
			contactos.add(bean);
		}
		ciudadano.setContactos(contactos);
		
		List<DireccionBean> direcciones = new ArrayList<DireccionBean>();
		for (DireccionDto d : dto.getDirecciones()) {
			DireccionBean bean = new DireccionBean();
			bean.setDireccionId(d.getDireccionId());
			bean.setDescripcion(d.getDescripcion());
			// Falta establecer Geography
			
			/*
			Coordinate[] coords = new Coordinate[1];
			coords[0] = new Coordinate(d.getLongitud(), d.getLatitud());
		    CoordinateArraySequence coordArraySeq = new CoordinateArraySequence(coords);
		    GeometryFactory factory4326 = new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING), 4326);
			//GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);
			Point ubicacion = new Point(coordArraySeq, factory4326);*/
			
			bean.setUbicacion(GeometriesFactory.createPoint(d.getLatitud(), d.getLongitud()));
			
			direcciones.add(bean);
		}
		ciudadano.setDirecciones(direcciones);
		
		List<SintomaBean> sintomas = new ArrayList<SintomaBean>();
		for(SintomaDto s : dto.getSintomas()) {
			SintomaBean bean = new SintomaBean();
			bean.setSintomaId(s.getSintomaId());
			sintomas.add(bean);
		}
		ciudadano.setSintomas(sintomas);
		
		List<SituacionRiesgoBean> situacionesRiesgo = new ArrayList<SituacionRiesgoBean>();
		for(SituacionRiesgoDto s : dto.getSituacionesRiesgo()) {
			SituacionRiesgoBean bean = new SituacionRiesgoBean();
			bean.setSituacionRiesgoId(s.getSituacionRiesgoId());
			situacionesRiesgo.add(bean);
		}
		ciudadano.setSituacionesRiesgo(situacionesRiesgo);
		
		return dao.insertar(ciudadano);
	}

	public CiudadanoBean actualizar(CiudadanoTranDto dto) {
		CiudadanoBean ciudadano = new CiudadanoBean();
		
		List<ContactoBean> contactos = new ArrayList<ContactoBean>();
		for (ContactoDto c : dto.getContactos()) {
			ContactoBean bean = new ContactoBean();
			bean.setContactoId(c.getContactoId());
			bean.setNombreCompleto(c.getNombreCompleto());
			bean.setParentesco(c.getParentesco());
			bean.setTelefono(c.getTelefono());
			bean.setCorreo(c.getCorreo());
			
			contactos.add(bean);
		}
		ciudadano.setContactos(contactos);
		
		List<DireccionBean> direcciones = new ArrayList<DireccionBean>();
		for (DireccionDto d : dto.getDirecciones()) {
			DireccionBean bean = new DireccionBean();
			bean.setDireccionId(d.getDireccionId());
			bean.setDescripcion(d.getDescripcion());
			// Falta establecer Geography
			
			direcciones.add(bean);
		}
		ciudadano.setDirecciones(direcciones);
		
		List<SintomaBean> sintomas = new ArrayList<SintomaBean>();
		for(SintomaDto s : dto.getSintomas()) {
			SintomaBean bean = new SintomaBean();
			
			SintomaBeanId id = new SintomaBeanId(s.getSintomaId(), s.getCiudadanoId());
			bean.setId(id);
			sintomas.add(bean);
		}
		ciudadano.setSintomas(sintomas);
		
		List<SituacionRiesgoBean> situacionesRiesgo = new ArrayList<SituacionRiesgoBean>();
		for(SituacionRiesgoDto s : dto.getSituacionesRiesgo()) {
			SituacionRiesgoBean bean = new SituacionRiesgoBean();
			
			SituacionRiesgoBeanId id = new SituacionRiesgoBeanId(s.getSituacionRiesgoId(), ciudadano.getCiudadanoId());
			bean.setId(id);
			situacionesRiesgo.add(bean);
		}
		ciudadano.setSituacionesRiesgo(situacionesRiesgo);
		
		return dao.actualizar(ciudadano);
	}	
}

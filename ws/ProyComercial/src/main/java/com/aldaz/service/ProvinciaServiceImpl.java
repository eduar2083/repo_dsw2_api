package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.ProvinciaDAO;
import com.aldaz.entity.ProvinciaBean;

@Service
public class ProvinciaServiceImpl implements ProvinciaService {
	
	private @Autowired ProvinciaDAO dao;

	public List<ProvinciaBean> listarXDepartamento(int departamentoId) {
		return dao.listarXDepartamento(departamentoId);
	}

}

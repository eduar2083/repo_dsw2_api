package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.PaisDAO;
import com.aldaz.entity.PaisBean;

@Service
public class PaisServiceImpl implements PaisService {
	
	@Autowired
	private PaisDAO dao;

	public List<PaisBean> listar() {
		return dao.listar();
	}

}

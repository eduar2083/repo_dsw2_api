package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.TipoDocumentoIdentidadDAO;
import com.aldaz.entity.TipoDocumentoIdentidadBean;

@Service
public class TipoDocumentoIdentidadServiceImpl implements TipoDocumentoIdentidadService {
	
	private @Autowired TipoDocumentoIdentidadDAO dao;

	public List<TipoDocumentoIdentidadBean> listar() {
		return dao.listar();
	}

}

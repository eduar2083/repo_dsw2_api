package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.DistritoDAO;
import com.aldaz.entity.DistritoBean;

@Service
public class DistritoServiceImpl implements DistritoService {
	
	private @Autowired DistritoDAO dao;

	public List<DistritoBean> listarXProvincia(int provinciaId) {
		return dao.listarXProvincia(provinciaId);
	}

}

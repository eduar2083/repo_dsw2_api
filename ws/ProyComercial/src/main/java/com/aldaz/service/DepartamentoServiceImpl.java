package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.DepartamentoDAO;
import com.aldaz.entity.DepartamentoBean;

@Service
public class DepartamentoServiceImpl implements DepartamentoService {

	private @Autowired DepartamentoDAO dao;
	
	public List<DepartamentoBean> listar() {
		return dao.listar();
	}

}

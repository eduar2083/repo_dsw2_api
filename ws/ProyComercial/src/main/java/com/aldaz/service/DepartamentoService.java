package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.DepartamentoBean;

public interface DepartamentoService {

	public List<DepartamentoBean> listar();
}

package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.DistritoBean;

public interface DistritoService {

	public List<DistritoBean> listarXProvincia(int provinciaId);
}

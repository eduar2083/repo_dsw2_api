package com.aldaz.util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

public class GeometriesFactory {

	private static GeometryFactory factory4326 = new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING), 4326);

	public static Point createPoint(Double pLatitude, Double pLongitude){
		return factory4326.createPoint(new Coordinate(pLongitude, pLatitude));
	}
	
	public static LineString createLineString(Coordinate[] pCoordsArray){
		return factory4326.createLineString(pCoordsArray);
	}
	
}
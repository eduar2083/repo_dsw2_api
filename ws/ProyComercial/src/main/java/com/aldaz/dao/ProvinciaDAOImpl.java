package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.ProvinciaBean;

@Repository
public class ProvinciaDAOImpl implements ProvinciaDAO {

	@SuppressWarnings("unchecked")
	public List<ProvinciaBean> listarXDepartamento(int departamentoId) {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from ProvinciaBean where departamentoId = ?1";
			query = session.createQuery(hql);
			
			query.setParameter(1, departamentoId);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}

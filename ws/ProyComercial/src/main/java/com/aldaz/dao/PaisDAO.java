package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.PaisBean;

public interface PaisDAO {
	public List<PaisBean> listar();
}

package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.TipoDocumentoIdentidadBean;

public interface TipoDocumentoIdentidadDAO {
	public List<TipoDocumentoIdentidadBean> listar();
}

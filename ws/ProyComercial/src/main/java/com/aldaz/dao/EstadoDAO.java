package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.EstadoBean;

public interface EstadoDAO {
	
	public List<EstadoBean> listar();

}

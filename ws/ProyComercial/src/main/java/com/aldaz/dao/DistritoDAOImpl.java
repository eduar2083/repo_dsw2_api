package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.DistritoBean;

@Repository
public class DistritoDAOImpl implements DistritoDAO {

	@SuppressWarnings("unchecked")
	public List<DistritoBean> listarXProvincia(int provinciaId) {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from DistritoBean where provinciaId = ?1";
			query = session.createQuery(hql);
			
			query.setParameter(1, provinciaId);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}

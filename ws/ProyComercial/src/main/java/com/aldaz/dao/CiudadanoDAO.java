package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.CiudadanoBean;

public interface CiudadanoDAO {

	public List<CiudadanoBean> filtrar(String nombre, String nroDocumentoIdentidad, int estadoId);
	
	public CiudadanoBean obtener(int ciudadanoId);
	
	public CiudadanoBean insertar(CiudadanoBean bean);
	
	public CiudadanoBean actualizar(CiudadanoBean bean);
}

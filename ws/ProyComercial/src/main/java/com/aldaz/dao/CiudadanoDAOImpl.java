package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.CiudadanoBean;
import com.aldaz.entity.ContactoBean;
import com.aldaz.entity.DireccionBean;
import com.aldaz.entity.SintomaBean;
import com.aldaz.entity.SintomaBeanId;
import com.aldaz.entity.SituacionRiesgoBean;
import com.aldaz.entity.SituacionRiesgoBeanId;

@Repository
public class CiudadanoDAOImpl implements CiudadanoDAO {

	@SuppressWarnings("unchecked")
	public List<CiudadanoBean> filtrar(String nombre, String nroDocumentoIdentidad, int estadoId) {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from CiudadanoBean WHERE (:nombre = '' or nombre like CONCAT('%', :nombre, '%')) AND (:nroDoc is null or nroDocumentoIdentidad like CONCAT('%', :nroDoc, '%')) AND (:estadoId = 0 or estadoId = :estadoId)";
			
			query = session.createQuery(hql);
			query.setParameter("nombre", nombre == null ? "" : nombre);
			query.setParameter("nroDoc", nroDocumentoIdentidad == null ? "" : nroDocumentoIdentidad);
			query.setParameter("estadoId", estadoId);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public CiudadanoBean obtener(int ciudadanoId) {
		Session session = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			return session.get(CiudadanoBean.class, ciudadanoId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public CiudadanoBean insertar(CiudadanoBean bean) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            // Grabar ciudadano
            session.save(bean);
            
            // Grabar contactos
            for (ContactoBean c : bean.getContactos()) {
            	c.setCiudadano(bean);
            	session.save(c);
            }
            
            // Grabar direcciones
            /*for (DireccionBean d : bean.getDirecciones()) {
            	d.setCiudadano(bean);
            	session.save(d);
            }*/
            
            // Grabar s�ntomas
            for (SintomaBean s : bean.getSintomas()) {
            	SintomaBeanId id = new SintomaBeanId(s.getSintomaId(), bean.getCiudadanoId());
            	s.setId(id);
            	session.save(s);
            }
            
            // Grabar situaciones de riesgo
            for (SituacionRiesgoBean s : bean.getSituacionesRiesgo()) {
            	SituacionRiesgoBeanId id = new SituacionRiesgoBeanId(s.getSituacionRiesgoId(), bean.getCiudadanoId());
            	s.setId(id);
            	session.save(s);
            }
            
            // Confirmar transacci�n
            transaction.commit();
            
            return bean;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}

	public CiudadanoBean actualizar(CiudadanoBean bean) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            CiudadanoBean entidad = session.get(CiudadanoBean.class, bean.getCiudadanoId());
            entidad.setNombre(bean.getNombre());
            entidad.setApellidoPaterno(bean.getApellidoPaterno());
            entidad.setApellidoMaterno(bean.getApellidoMaterno());
            entidad.setSexo(bean.isSexo());
            entidad.setNacionalidad(bean.getNacionalidad());
            entidad.setTipoDocumento(bean.getTipoDocumento());
            entidad.setNroDocumentoIdentidad(bean.getNroDocumentoIdentidad());
            entidad.setCelular(bean.getCelular());
            entidad.setCorreo(bean.getCorreo());
            entidad.setFechaNacimiento(bean.getFechaNacimiento());
            entidad.setEstado(bean.getEstado());
            transaction.commit();
            
            return bean;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}

}

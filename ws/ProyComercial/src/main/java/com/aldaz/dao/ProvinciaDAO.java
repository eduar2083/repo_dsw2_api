package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.ProvinciaBean;

public interface ProvinciaDAO {

	public List<ProvinciaBean> listarXDepartamento(int departamentoId);
}

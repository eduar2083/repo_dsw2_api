package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.EstadoBean;

@Repository
public class EstadoDAOImpl implements EstadoDAO {

	@SuppressWarnings("unchecked")
	public List<EstadoBean> listar() {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from EstadoBean";
			query = session.createQuery(hql);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}

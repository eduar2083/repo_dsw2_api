package com.aldaz.dao;

import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.ProductoBean;

@Repository
public class ProductoDAOImpl implements ProductoDAO {
	
	public ProductoBean insertar(ProductoBean bean) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(bean);
            transaction.commit();
            
            return bean;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}
	
	public void actualizar(ProductoBean bean) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            ProductoBean entidad = session.get(ProductoBean.class, bean.getProductoId());
            entidad.setNombre(bean.getNombre());
            entidad.setMarca(bean.getMarca());
            entidad.setPrecio(bean.getPrecio());
            entidad.setStock(bean.getStock());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}
	
	public void eliminar(int idProducto) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            ProductoBean bean = session.get(ProductoBean.class, idProducto);
            session.delete(bean);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}
	
	public ProductoBean obtenerPorId(int idProducto) {
		Session session = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			return session.get(ProductoBean.class, idProducto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ProductoBean> listar() {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from ProductoBean";
			query = session.createQuery(hql);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProductoBean> filtrarXPrecio(double minimo, double maximo) {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from ProductoBean Where precio between ?1 and ?2";
			query = session.createQuery(hql);
			query.setParameter(1, minimo);
			query.setParameter(2, maximo);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}

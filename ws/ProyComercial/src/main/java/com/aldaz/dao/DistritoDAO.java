package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.DistritoBean;

public interface DistritoDAO {

	public List<DistritoBean> listarXProvincia(int provinciaId);
}

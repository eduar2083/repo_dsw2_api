package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.TipoDocumentoIdentidadBean;

@Repository
public class TipoDocumentoIdentidadDAOImpl implements TipoDocumentoIdentidadDAO {

	@SuppressWarnings("unchecked")
	public List<TipoDocumentoIdentidadBean> listar() {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from TipoDocumentoIdentidadBean";
			query = session.createQuery(hql);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}

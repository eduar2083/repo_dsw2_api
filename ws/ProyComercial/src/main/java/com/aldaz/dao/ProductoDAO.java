package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.ProductoBean;

public interface ProductoDAO {
	
	public ProductoBean insertar(ProductoBean bean);
	
	public void actualizar(ProductoBean bean);
	
	public void eliminar(int idProducto);
	
	public ProductoBean obtenerPorId(int idProducto);
	
	public List<ProductoBean> listar();

	public List<ProductoBean> filtrarXPrecio(double minimo, double maximo);
}

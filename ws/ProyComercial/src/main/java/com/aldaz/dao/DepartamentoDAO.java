package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.DepartamentoBean;

public interface DepartamentoDAO {

	public List<DepartamentoBean> listar();
}
